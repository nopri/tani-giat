<?php

    include './config/koneksi.php';

    $sql=mysqli_query($con,"SELECT *, fullname as publisher, DATE_FORMAT(publish_date, '%d-%m-%Y') as tgl FROM tbl_transportasi a INNER JOIN tbl_user b ON a.id_user = b.id_user ORDER BY publish_date DESC");

    if(isset($_GET["id"])){
        $id=$_GET["id"];

        $sql=mysqli_query($con,"SELECT * FROM tbl_transportasi WHERE id_transportasi='$id'");
    }

    $response=array();
    $cek=mysqli_num_rows($sql);
    if($cek >0){
        $response["berita"]=array();

        while ($row=mysqli_fetch_array($sql)){

            $data=array();
            $data["id_transportasi"]=$row["id_transportasi"];
            $data["nama"]=$row["nama"];
            $data["deskripsi"]=$row["deskripsi"];
            $data["foto"]=$row["foto"];
            $data["publisher"]=$row["publisher"];
            $data["publish_date"]=$row["tgl"];
            $data["update_date"]=$row["update_date"];
            
            $response["msg"]="News found.";
            $response["code"]=200;
            $response["status"]=true;    
            array_push($response["berita"],$data);
        }

        echo json_encode($response);

    }else{
        $response["msg"]="News not found.";
        $response["code"]=404;
        $response["status"]=false; 
        echo json_encode($response);
    } 

?>
