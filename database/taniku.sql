-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2019 at 01:15 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taniku`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catatan`
--

CREATE TABLE `tbl_catatan` (
  `idCatatan` int(11) NOT NULL,
  `triwulan` varchar(50) NOT NULL,
  `rencanaTanam` varchar(50) NOT NULL,
  `penyiapanLahan` varchar(50) NOT NULL,
  `penanaman` varchar(50) NOT NULL,
  `pemeliharaan` varchar(50) NOT NULL,
  `panen` varchar(20) NOT NULL,
  `pascaPanen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_catatan`
--

INSERT INTO `tbl_catatan` (`idCatatan`, `triwulan`, `rencanaTanam`, `penyiapanLahan`, `penanaman`, `pemeliharaan`, `panen`, `pascaPanen`) VALUES
(14, 'cabau', 'cvv', 'vbb', 'bb', 'bng', 'vvv', 'bvb');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transportasi`
--

CREATE TABLE `tbl_transportasi` (
  `nama` varchar(150) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` text NOT NULL,
  `id_user` int(3) NOT NULL,
  `publish_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_transportasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transportasi`
--

INSERT INTO `tbl_transportasi` (`nama`, `deskripsi`, `foto`, `id_user`, `publish_date`, `update_date`, `id_transportasi`) VALUES
('Mitsubishi Diesel', 'Muatan 3-5 ton', 'IMG_20190513_193123.jpg', 1, '2019-05-15 01:34:04', '2019-05-15 01:34:21', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `fullname` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `access` varchar(100) NOT NULL DEFAULT 'reader',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_active_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`fullname`, `username`, `password`, `access`, `insert_date`, `update_date`, `last_active_date`, `id_user`) VALUES
('Nopri Yendry', 'nopri', '827ccb0eea8a706c4c34a16891f84e7b', 'admin', '2019-05-05 14:19:52', '2019-05-05 19:07:11', '2019-05-05 19:07:11', 1),
('Zee Rey', 'zee', 'e10adc3949ba59abbe56e057f20f883e', 'reader', '2019-05-05 19:21:42', '2019-05-05 20:49:05', '2019-05-05 20:49:05', 2),
('Delta', 'delta', '827ccb0eea8a706c4c34a16891f84e7b', 'reader', '2019-05-05 20:22:22', '2019-05-05 20:22:22', '2019-05-05 20:22:22', 3),
('Layla', 'layla', '827ccb0eea8a706c4c34a16891f84e7b', 'reader', '2019-05-06 02:17:24', '2019-05-06 02:17:24', '2019-05-06 02:17:24', 4),
('Sweta', 'sweta', 'ffc6edb9aeaa422471896f32de2d7516', 'reader', '2019-05-10 01:22:02', '2019-05-10 01:22:02', '2019-05-10 01:22:02', 5),
('Anggiat', 'anggiat', '1889e7c4d2a2e7bd5c899955f4f6bf1c', 'reader', '2019-05-10 02:15:26', '2019-05-10 02:15:26', '2019-05-10 02:15:26', 6),
('Obria', 'obrian', 'b384feda9793ce89b4e9b9c1f310482e', 'reader', '2019-05-13 03:58:24', '2019-05-13 03:58:24', '2019-05-13 03:58:24', 7),
('diana', 'diana', '3a23bb515e06d0e944ff916e79a7775c', 'reader', '2019-05-15 08:21:09', '2019-05-15 08:21:09', '2019-05-15 08:21:09', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_catatan`
--
ALTER TABLE `tbl_catatan`
  ADD PRIMARY KEY (`idCatatan`);

--
-- Indexes for table `tbl_transportasi`
--
ALTER TABLE `tbl_transportasi`
  ADD PRIMARY KEY (`id_transportasi`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_catatan`
--
ALTER TABLE `tbl_catatan`
  MODIFY `idCatatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_transportasi`
--
ALTER TABLE `tbl_transportasi`
  MODIFY `id_transportasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
