package comn.example.itd_stu.tanigiat.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import comn.example.itd_stu.tanigiat.activity.CatatanActivity;
import comn.example.itd_stu.tanigiat.R;
import comn.example.itd_stu.tanigiat.model.DataModel;


public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData> {

    private List<DataModel> mList ;
    private Context ctx;


    public  AdapterData (Context ctx, List<DataModel> mList)
    {
        this.ctx = ctx;
        this.mList = mList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layoutlist,parent, false);
        HolderData holder = new HolderData(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        DataModel dm = mList.get(position);
        holder.triwulan.setText(dm.getTriwulan());
        holder.rencanaTanam.setText(dm.getRencanatanam());
        holder.penyiapanLahan.setText(dm.getPenyiapanlahan());
        holder.penanaman.setText(dm.getPenanaman());
        holder.pemeliharaan.setText(dm.getPemeliharaan());
        holder.panen.setText(dm.getPanen());
        holder.pascaPanen.setText(dm.getPascapanen());
        holder.dm = dm;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    class HolderData extends  RecyclerView.ViewHolder{
        TextView triwulan, rencanaTanam, penyiapanLahan, penanaman,pemeliharaan, panen, pascaPanen ;
        DataModel dm;
        public HolderData (View v)
        {
            super(v);

            triwulan  = (TextView) v.findViewById(R.id.tvTriwulan);
            rencanaTanam = (TextView) v.findViewById(R.id.tvRencanaTanam);
            penyiapanLahan = (TextView) v.findViewById(R.id.tvPenyiapanLahan);
            penanaman = (TextView) v.findViewById(R.id.tvPenanaman);
            pemeliharaan = (TextView) v.findViewById(R.id.tvPemeliharaan);
            panen = (TextView) v.findViewById(R.id.tvPanen);
            pascaPanen = (TextView) v.findViewById(R.id.tvPascaPanen);


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent goInput = new Intent(ctx,CatatanActivity.class);
                    goInput.putExtra("idCatatan", dm.getIdCatatan());
                    goInput.putExtra("triwulan", dm.getTriwulan());
                    goInput.putExtra("rencanaTanam", dm.getRencanatanam());
                    goInput.putExtra("penyiapanLahan", dm.getPenyiapanlahan());
                    goInput.putExtra("penanaman", dm.getPenanaman());
                    goInput.putExtra("pemeliharaan", dm.getPemeliharaan());
                    goInput.putExtra("panen", dm.getPanen());
                    goInput.putExtra("pascaPanen", dm.getPascapanen());




                    ctx.startActivity(goInput);
                }
            });
        }
    }
}