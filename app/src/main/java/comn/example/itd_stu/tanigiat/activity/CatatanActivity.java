package comn.example.itd_stu.tanigiat.activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import comn.example.itd_stu.tanigiat.R;
import comn.example.itd_stu.tanigiat.api.ApiRequestCatatan;
import comn.example.itd_stu.tanigiat.api.Retroserver;
import comn.example.itd_stu.tanigiat.model.ResponsModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatatanActivity extends AppCompatActivity {
    EditText triwulan, rencanaTanam, penyiapanLahan, penanaman, pemeliharaan, panen, pascaPanen;
    Button btnsave, btnTampildata, btnupdate, btndelete;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catatan);


        triwulan = (EditText) findViewById(R.id.edt_triwulan);
        rencanaTanam = (EditText) findViewById(R.id.edt_rencanatanam);
        penyiapanLahan = (EditText) findViewById(R.id.edt_penyiapanlahan);
        penanaman = (EditText) findViewById(R.id.edt_penanaman);
        pemeliharaan = (EditText) findViewById(R.id.edt_pemeliharaan);
        panen = (EditText) findViewById(R.id.edt_panen);
        pascaPanen = (EditText) findViewById(R.id.edt_pascapanen);

        btnTampildata = (Button) findViewById(R.id.btntampildata);
        btnupdate = (Button) findViewById(R.id.btnUpdate);
        btnsave = (Button) findViewById(R.id.btn_insertdata);
        btndelete = (Button) findViewById(R.id.btnhapus);

        Intent data = getIntent();
        final String idCatatan = data.getStringExtra("idCatatan");
        if (idCatatan != null) {
            btnsave.setVisibility(View.GONE);
            btnTampildata.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);
            triwulan.setText(data.getStringExtra("triwulan"));
            rencanaTanam.setText(data.getStringExtra("rencanaTanam"));
            penyiapanLahan.setText(data.getStringExtra("penyiapanLahan"));
            penanaman.setText(data.getStringExtra("penanaman"));
            pemeliharaan.setText(data.getStringExtra("pemeliharaan"));
            panen.setText(data.getStringExtra("panen"));
            pascaPanen.setText(data.getStringExtra("pascaPanen"));

        }

        pd = new ProgressDialog(this);


        btnTampildata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(CatatanActivity.this,  comn.example.itd_stu.tanigiat.activity.TampilData.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                ApiRequestCatatan api = Retroserver.getClient().create(ApiRequestCatatan.class);
                Call<ResponsModel> del = api.deleteData(idCatatan);
                del.enqueue(new Callback<ResponsModel>() {
                    @Override
                    public void onResponse(Call<ResponsModel> call, Response<ResponsModel> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(CatatanActivity.this, response.body().getPesan(), Toast.LENGTH_SHORT).show();
                        Intent gotampil = new Intent(CatatanActivity.this, TampilData.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("update ....");
                pd.setCancelable(false);
                pd.show();

                ApiRequestCatatan api = Retroserver.getClient().create(ApiRequestCatatan.class);
                Call<ResponsModel> update = api.updateData(idCatatan, triwulan.getText().toString(), rencanaTanam.getText().toString(), penyiapanLahan.getText().toString(), penanaman.getText().toString(), pemeliharaan.getText().toString(), panen.getText().toString(), pascaPanen.getText().toString());
                update.enqueue(new Callback<ResponsModel>() {
                    @Override
                    public void onResponse(Call<ResponsModel> call, Response<ResponsModel> response) {
                        Log.d("Retro", "Response");
                        Toast.makeText(CatatanActivity.this, response.body().getPesan(), Toast.LENGTH_SHORT).show();
                        pd.hide();
                    }

                    @Override
                    public void onFailure(Call<ResponsModel> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "OnFailure");

                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ... ");
                pd.setCancelable(false);
                pd.show();

                String striwulan = triwulan.getText().toString();
                String srencanaTanam = rencanaTanam.getText().toString();
                String spenyiapanLahan = penyiapanLahan.getText().toString();
                String spenanaman = penanaman.getText().toString();
                String spemeliharaan = pemeliharaan.getText().toString();
                String spanen = panen.getText().toString();
                String spascaPanen = pascaPanen.getText().toString();




                ApiRequestCatatan api = Retroserver.getClient().create(ApiRequestCatatan.class);

                Call<ResponsModel> sendbio = api.sendBiodata(striwulan, srencanaTanam, spenyiapanLahan, spenanaman, spemeliharaan, spanen, spascaPanen);
                sendbio.enqueue(new Callback<ResponsModel>() {
                    @Override
                    public void onResponse(Call<ResponsModel> call, Response<ResponsModel> response) {
                        pd.hide();
                        Log.d("RETRO", "response : " + response.body().toString());
                        String kode = response.body().getKode();

                        if (kode.equals("1")) {
                            Toast.makeText(CatatanActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CatatanActivity.this, "Data Error tidak berhasil disimpan", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Falure : " + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}
