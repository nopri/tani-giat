package comn.example.itd_stu.tanigiat.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import comn.example.itd_stu.tanigiat.model.ResponsModel;


public interface ApiRequestCatatan {
    @FormUrlEncoded
    @POST("insert.php")
        Call<ResponsModel> sendBiodata (@Field("triwulan") String triwulan,
                                       @Field("rencanaTanam") String rencanaTanam,
                                       @Field("penyiapanLahan") String penyiapanLahan,
                                        @Field("penanaman") String penanaman,
                                        @Field("pemeliharaan") String pemeliharaan,
                                        @Field("panen") String panen,
                                        @Field("pascaPanen") String pascaPanen);

    @GET("read.php")
    Call<ResponsModel> getBiodata();


    @FormUrlEncoded
    @POST("update.php")
    Call<ResponsModel> updateData( @Field("idCatatan") String idCatatan,
                                   @Field("triwulan") String triwulan,
                                   @Field("rencanaTanam") String rencanaTanam,
                                   @Field("penyiapanLahan") String penyiapanLahan,
                                   @Field("penanaman") String penanaman,
                                   @Field("pemeliharaan") String pemeliharaan,
                                   @Field("panen") String panen,
                                   @Field("pascaPanen") String pascaPanen);


    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponsModel> deleteData(@Field("idCatatan") String idCatatan);
}
