package comn.example.itd_stu.tanigiat.network;

import comn.example.itd_stu.tanigiat.config.MyConstant;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ITD-STU on 5/5/2019.
 */

public class InitRetrofit {
    public static Retrofit setInit() {
        return new Retrofit.Builder()
                .baseUrl(MyConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiServices getInstance() {
        return setInit().create(ApiServices.class);
    }
}

