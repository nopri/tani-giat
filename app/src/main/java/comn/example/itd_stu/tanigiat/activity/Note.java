package comn.example.itd_stu.tanigiat.activity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by ITD-STU on 4/13/2019.
 */
@Entity(tableName = "note_table")

public class Note {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String rencanatanam;
    private String penyiapanlahan;
    private String penanaman;
    private String pemeliharaan;
    private String panen;
    private String pascapanen;

    public Note(String rencanatanam, String penyiapanlahan, String penanaman, String pemeliharaan, String panen, String pascapanen) {
        this.rencanatanam = rencanatanam;
        this.penyiapanlahan = penyiapanlahan;
        this.penanaman = penanaman;
        this.pemeliharaan = pemeliharaan;
        this.panen = panen;
        this.pascapanen = pascapanen;

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getRencanatanam() {
        return rencanatanam;
    }

    public String getPenyiapanlahan() {
        return penyiapanlahan;
    }

    public String getPenanaman() {
        return penanaman;
    }

    public String getPemeliharaan() {
        return pemeliharaan;
    }

    public String getPanen() {
        return panen;
    }

    public String getPascapanen() {
        return pascapanen;
    }


}
