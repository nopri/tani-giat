package comn.example.itd_stu.tanigiat.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import comn.example.itd_stu.tanigiat.R;

public class AddNoteActivity3 extends AppCompatActivity {
    public static final String EXTRA_RENCANATANAM =
            "comn.example.itd_stu.roomviewmodellivedata.EXTRA_RENCANATANAM";
    public static final String EXTRA_PENYIAPANLAHAN =
            "comn.example.itd_stu.roomviewmodellivedata.EXTRA_PENYIAPANLAHAN";
    public static final String EXTRA_PENANAMAN =
            "comn.example.itd_stu.roomviewmodellivedata.EXTRA_PENANAMAN";
    public static final String EXTRA_PEMELIHARAAN =
            "comn.example.itd_stu.roomviewmodellivedata.EXTRA_PEMELIHARAAN";
    public static final String EXTRA_PANEN =
            "comn.example.itd_stu.roomviewmodellivedata.EXTRA_PANEN";
    public static final String EXTRA_PASCAPANEN =
            "comn.example.itd_stu.roomviewmodellivedata.EXTRA_PASCAPANEN";

    private EditText editTextRencanatanam;
    private EditText editTextPenyiapanlahan;
    private EditText editTextPenanaman;
    private EditText editTextPemeliharaan;
    private EditText editTextPanen;
    private EditText editTextPascapanen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note3);
        editTextRencanatanam = findViewById(R.id.edit_text_rencanatanam);
        editTextPenyiapanlahan = findViewById(R.id.edit_text_penyiapanlahan);
        editTextPenanaman = findViewById(R.id.edit_text_penanaman);
        editTextPemeliharaan = findViewById(R.id.edit_text_pemeliharaan);
        editTextPanen = findViewById(R.id.edit_text_panen);
        editTextPascapanen = findViewById(R.id.edit_text_pascapanen);



        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        setTitle("Add Note");
    }

    private void saveNote() {
        String rencanatanam = editTextRencanatanam.getText().toString();
        String penyiapanlahan = editTextPenyiapanlahan.getText().toString();
        String penanaman = editTextPenanaman.getText().toString();
        String pemeliharaan = editTextPemeliharaan.getText().toString();
        String panen = editTextPanen.getText().toString();
        String pascapanen = editTextPascapanen.getText().toString();


        if (rencanatanam.trim().isEmpty() || penyiapanlahan.trim().isEmpty() || penanaman.trim().isEmpty() || pemeliharaan.trim().isEmpty() || panen.trim().isEmpty() || pascapanen.trim().isEmpty())  {
            Toast.makeText(this, "Please insert the field", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_RENCANATANAM,rencanatanam);
        data.putExtra(EXTRA_PENYIAPANLAHAN, penyiapanlahan);
        data.putExtra(EXTRA_PENANAMAN,penanaman);
        data.putExtra(EXTRA_PEMELIHARAAN,pemeliharaan);
        data.putExtra(EXTRA_PANEN,panen);
        data.putExtra(EXTRA_PASCAPANEN,pascapanen);

        setResult(RESULT_OK,data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_note:
                saveNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}