package comn.example.itd_stu.tanigiat.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import comn.example.itd_stu.tanigiat.R;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener{
    LinearLayout pria;
    LinearLayout wanita;
    LinearLayout anak;
    LinearLayout pasangan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        pria = findViewById(R.id.diana);
        pria.setOnClickListener(this);
        wanita = findViewById(R.id.boy);
        wanita.setOnClickListener(this);
        anak = findViewById(R.id.nopri);
        anak.setOnClickListener(this);
        pasangan = findViewById(R.id.vicki);
        pasangan.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.diana:
                Intent moveIntent = new Intent(AboutActivity.this, Diana.class);
                startActivity(moveIntent);
                break;
            case R.id.boy:
                Intent moveIntent2 = new Intent(AboutActivity.this, Boy.class);
                startActivity(moveIntent2);
                break;
            case R.id.nopri:
                Intent moveIntent3 = new Intent(AboutActivity.this, Nopri.class);
                startActivity(moveIntent3);
                break;
            case R.id.vicki:
                Intent moveIntent4 = new Intent(AboutActivity.this, Vicki.class);
                startActivity(moveIntent4);
                break;
        }

    }

}
