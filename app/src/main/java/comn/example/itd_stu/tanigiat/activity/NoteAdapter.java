package comn.example.itd_stu.tanigiat.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import comn.example.itd_stu.tanigiat.R;

/**
 * Created by ITD-STU on 4/13/2019.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {
    private List<Note> notes = new ArrayList<>();

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);
        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NoteHolder holder, int position) {
        Note currentNote = notes.get(position);
        holder.textViewRencanatanam.setText(currentNote.getRencanatanam());
        holder.textViewPenyiapanlahan.setText(currentNote.getPenyiapanlahan());
        holder.textViewPenanaman.setText(currentNote.getPenanaman());
        holder.textViewPemeliharaan.setText(currentNote.getPemeliharaan());
        holder.textViewPanen.setText(currentNote.getPanen());
        holder.textViewPascapanen.setText(currentNote.getPascapanen());
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    public Note getNoteAt(int position) {
        return notes.get(position);
    }

    class NoteHolder extends RecyclerView.ViewHolder {
        private TextView textViewRencanatanam;
        private TextView textViewPenyiapanlahan;
        private TextView textViewPenanaman;
        private TextView textViewPemeliharaan;
        private TextView textViewPanen;
        private TextView textViewPascapanen;


        public NoteHolder(View itemView) {
            super(itemView);
            textViewRencanatanam = itemView.findViewById(R.id.text_view_rencanatanam);
            textViewPenyiapanlahan = itemView.findViewById(R.id.text_view_penyiapanlahan);
            textViewPenanaman = itemView.findViewById(R.id.text_view_penanaman);
            textViewPemeliharaan = itemView.findViewById(R.id.text_view_pemeliharaan);
            textViewPanen = itemView.findViewById(R.id.text_view_panen);
            textViewPascapanen = itemView.findViewById(R.id.text_view_pascapanen);
        }
    }

}
