package comn.example.itd_stu.tanigiat.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import comn.example.itd_stu.tanigiat.R;

public class CatatanTaniActivity extends AppCompatActivity implements View.OnClickListener{
    private CardView triwulan1, triwulan2, triwulan3, triwulan4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catatan_tani);
        triwulan1 = (CardView) findViewById(R.id.triwulan1);
        triwulan2 = (CardView) findViewById(R.id.triwulan2);
        triwulan3 = (CardView) findViewById(R.id.triwulan3);
        triwulan4 = (CardView) findViewById(R.id.triwulan4);

        triwulan1.setOnClickListener(this);
        triwulan2.setOnClickListener(this);
        triwulan3.setOnClickListener(this);
        triwulan4.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()){
            case R.id.triwulan1: i=new Intent(this,CatatanActivity.class);startActivity(i);break;
            case R.id.triwulan2 : i=new Intent(this,MainNoteActivity.class);startActivity(i);break;
            case R.id.triwulan3 : i=new Intent(this,CatatanActivity.class);startActivity(i);break;
            case R.id.triwulan4 : i=new Intent(this,MainNoteActivity.class);startActivity(i);break;

        }


    }
}
