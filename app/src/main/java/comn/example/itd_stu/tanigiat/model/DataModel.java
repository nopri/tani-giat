package comn.example.itd_stu.tanigiat.model;


public class DataModel {
    String idCatatan, triwulan, rencanatanam, penyiapanlahan, penanaman, pemeliharaan, panen, pascapanen;


    public String getIdCatatan() {
        return idCatatan;
    }

    public void setIdCatatan(String idCatatan) {
        this.idCatatan = idCatatan;
    }

    public String getTriwulan() {
        return triwulan;
    }

    public void setTriwulan(String triwulan) {
        this.triwulan = triwulan;
    }

    public String getRencanatanam() {
        return rencanatanam;
    }

    public void setRencanatanam(String rencanatanam) {
        this.rencanatanam = rencanatanam;
    }

    public String getPenyiapanlahan() {
        return penyiapanlahan;
    }

    public void setPenyiapanlahan(String penyiapanlahan) {
        this.penyiapanlahan = penyiapanlahan;
    }

    public String getPenanaman() {
        return penanaman;
    }

    public void setPenanaman(String penanaman) {
        this.penanaman = penanaman;
    }

    public String getPemeliharaan() {
        return pemeliharaan;
    }

    public void setPemeliharaan(String pemeliharaan) {
        this.pemeliharaan = pemeliharaan;
    }

    public String getPanen() {
        return panen;
    }

    public void setPanen(String panen) {
        this.panen = panen;
    }

    public String getPascapanen() {
        return pascapanen;
    }

    public void setPascapanen(String pascapanen) {
        this.pascapanen = pascapanen;
    }

}
