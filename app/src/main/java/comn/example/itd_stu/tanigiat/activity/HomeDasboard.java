package comn.example.itd_stu.tanigiat.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import comn.example.itd_stu.tanigiat.R;
import comn.example.itd_stu.tanigiat.helpers.MyFunction;
import comn.example.itd_stu.tanigiat.helpers.SharedPrefManager;

public class HomeDasboard extends MyFunction implements View.OnClickListener {
    private final static String TAG = HomeDasboard.class.getSimpleName();
    private CardView catatanCard, hargaCard, transportasiCard, pasarCard, lahanCard;
    @BindView(R.id.llLayout)
    LinearLayout llLayout;
    SharedPrefManager sharedPrefManager;
    View parentView;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_dasboard);

        mContext = this;
        sharedPrefManager = new SharedPrefManager(this);
        parentView = llLayout;
        catatanCard = (CardView) findViewById(R.id.catatan);
        hargaCard = (CardView) findViewById(R.id.kelolaHarga);
        transportasiCard = (CardView) findViewById(R.id.transportasi);
        pasarCard = (CardView) findViewById(R.id.pasarTani);
        lahanCard = (CardView) findViewById(R.id.registrasiLahan);

        catatanCard.setOnClickListener(this);
        hargaCard.setOnClickListener(this);
        transportasiCard.setOnClickListener(this);
        pasarCard.setOnClickListener(this);
        lahanCard.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Intent i;
        switch (view.getId()) {
            case R.id.catatan:
                i = new Intent(this, CatatanTaniActivity.class);
                startActivity(i);
                break;
            case R.id.transportasi:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
                break;

            case R.id.registrasiLahan:
                i = new Intent(this, DaftarLahan.class);
                startActivity(i);
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.about) {
            startActivity(new Intent(this, AboutActivity.class));
        } else if (item.getItemId() == R.id.keluar) {
            startActivity(new Intent(this, LogoutActivity.class));
        }
        return true;
    }


}
