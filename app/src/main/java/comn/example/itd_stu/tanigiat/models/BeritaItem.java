package comn.example.itd_stu.tanigiat.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ITD-STU on 5/5/2019.
 */

public class BeritaItem {
    @SerializedName("foto")
    private String foto;

    @SerializedName("publisher")
    private String publisher;

    @SerializedName("nama")
    private String nama;

    @SerializedName("id_transportasi")
    private String idTransportasi;

    @SerializedName("publish_date")
    private String publishDate;

    @SerializedName("deskripsi")
    private String deskripsi;

    @SerializedName("update_date")
    private String updateDate;

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getFoto() {
        return foto;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setIdTransportasi(String idTransportasi) {
        this.idTransportasi = idTransportasi;
    }

    public String getIdTransportasi() {
        return idTransportasi;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    @Override
    public String toString() {
        return
                "BeritaItem{" +
                        "foto = '" + foto + '\'' +
                        ",publisher = '" + publisher + '\'' +
                        ",nama = '" + nama + '\'' +
                        ",id_transportasi = '" + idTransportasi + '\'' +
                        ",publish_date = '" + publishDate + '\'' +
                        ",deskripsi = '" + deskripsi + '\'' +
                        ",update_date = '" + updateDate + '\'' +
                        "}";
    }
}
